package com.legosoft.demo.customtask.test;

import java.util.Iterator;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.RuntimeEnvironmentBuilder;
import org.kie.api.runtime.manager.RuntimeManager;
import org.kie.api.runtime.manager.RuntimeManagerFactory;

import com.legosoft.demo.customtask.LegoCustomHandler;
import com.mx.legosoft.custom.services.LegoJavaService;

import bitronix.tm.resource.jdbc.PoolingDataSource;

public class ProcessMain {

	public static void main(String[] args) {
		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks.getKieClasspathContainer();
		KieBase kbase = kContainer.getKieBase("kbase");

		RuntimeManager manager = createRuntimeManager(kbase);
		RuntimeEngine engine = manager.getRuntimeEngine(null);
		KieSession ksession = engine.getKieSession();
		
		ksession.getWorkItemManager().registerWorkItemHandler("Legosoftcustomtask", new
				LegoCustomHandler());
		
		ksession.getWorkItemManager().registerWorkItemHandler("LegoJavaService", new
				LegoJavaService(ksession));
		
		
		System.out.println("nstaces"+ksession.getKieBase().getProcesses());
		
		Iterator<org.kie.api.definition.process.Process> iterator = ksession.getKieBase().getProcesses().iterator();
		 
        // while loop
        while (iterator.hasNext()) {
        System.out.println("value= " + iterator.next().getId());
       
        }
		
		ksession.startProcess("custom_task.Test_custom_task");

		manager.disposeRuntimeEngine(engine);
		System.exit(0);
	}

	private static RuntimeManager createRuntimeManager(KieBase kbase) {
		//JBPMHelper.startH2Server();
		//JBPMHelper.setupDataSource();
		setupDataSource();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("org.jbpm.persistence.jpa.eclipse");
		RuntimeEnvironmentBuilder builder = RuntimeEnvironmentBuilder.Factory.get()
			.newDefaultBuilder().entityManagerFactory(emf)
			.knowledgeBase(kbase);
		return RuntimeManagerFactory.Factory.get()
			.newSingletonRuntimeManager(builder.get(), "legosoft:custom_task:1.0.3");
	}

	 public static PoolingDataSource setupDataSource() {
	        // Please edit here when you want to use your database
	        PoolingDataSource pds = new PoolingDataSource();
	        pds.setUniqueName("jdbc/jbpm-ds");
	        pds.setClassName("bitronix.tm.resource.jdbc.lrc.LrcXADataSource");
	        pds.setMaxPoolSize(5);
	        pds.setAllowLocalTransactions(true);
	        pds.getDriverProperties().put("user", "legobpm");
	        pds.getDriverProperties().put("password", "legobpm");
	        pds.getDriverProperties().put("url", "jdbc:mysql://localhost:3306/jbpmlego");
	        pds.getDriverProperties().put("driverClassName", "com.mysql.jdbc.Driver");
	        pds.init();
	        return pds;
	    }
	
	
}